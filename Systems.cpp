#include "Systems.hpp"
#include "core_system/Config.hpp"
#include "Prefabs.hpp"
#include "HelperFunctions.hpp"
#include <cmath>
#include <string>
#include <iostream>
#include <filesystem>
namespace fs = std::filesystem;

using namespace sf;
using entity_type = entt::DefaultRegistry::entity_type;
//SYSTEM : MOVE
void SystemMove(float dt, entt::DefaultRegistry &registry) {
  registry.view<Name, Position, Velocity>().each([dt](auto entity, auto name, auto &position, auto &velocity) {
    position.x += velocity.dx * dt;
    position.y += velocity.dy * dt;
  });
}

//SYTEM : Sound
void SystemPlaySound(entt::DefaultRegistry &registry){
  static bool loaded = false;
  static sf::SoundBuffer buffer;
  static sf::Sound sound;
  if (!loaded) {
    if (buffer.loadFromFile("../resources/sounds/soundtest.wav")) {
      loaded = true;
      sound.setBuffer(buffer);
    }
  }

  registry.view<CoinFound>().each([&registry](auto entity, auto& coinFound) {
    coinFound.found = false;
    registry.remove<CoinFound>(entity);
    if (sound.getStatus() != sf::Sound::Playing) {
      sound.play();
    }
    std::cout << "Coin found sound " << std::endl;
  });
}

// SYSTEM : APPLY CHANGES
void SystemApply(entt::DefaultRegistry &registry) {
  registry
    .view<DrawableShape, Position, Size>()
    .each([](auto entity, DrawableShape &drawable, Position pos, Size size) {
      drawable.shape->setPosition({pos.x, pos.y});
    });

  registry
    .view<AnimatedSpriteComponent, Position>()
    .each([](auto entity, AnimatedSpriteComponent &animatedSprite, Position pos) {
      auto bounds = animatedSprite.sprite->getGlobalBounds();
      animatedSprite.sprite->setPosition({pos.x, pos.y});
    });
}

// SYSTEM : DRAW
void SystemDraw(RenderWindow &window, entt::DefaultRegistry &registry) {
  Shape * shape;
  registry.view<DrawableShape>().each([&](auto entity, auto &drawable) {
    shape = drawable.shape.get();
    shape->setFillColor(drawable.color);
    window.draw(*shape);
  });
}

void SystemDrawSprites(RenderWindow &window, entt::DefaultRegistry &registry) {
  registry.view<SpriteComponent>().each([&](auto entity, auto &spriteComponent) {
    auto& sprite = spriteComponent.sprite;
    window.draw(*sprite);
  });
}

void SystemDrawAnimatedSprites(RenderWindow &window, entt::DefaultRegistry &registry, float dt){
  const int SPRITE_SPEED_MS = 100;
  static float passedTime = 0.0f;
  passedTime += dt;

  registry.view<AnimatedSpriteComponent, AnimationStateComponent>().each([&](auto entity, auto &animatedSpriteComponent, auto &animationStateComponent) {
    auto& sprite = animatedSpriteComponent.sprite;
    auto& textures = animatedSpriteComponent.spriteTextures;
    int& currentFrame = animatedSpriteComponent.currentFrame;
    float speed = animatedSpriteComponent.speed;
    AnimationState currentState = animationStateComponent.currentState;
    sf::Vector2i bounds = animationStateComponent.states[currentState];

    bool advanceAnimation = false;
    if (passedTime > speed){
      passedTime = 0.0f;
      advanceAnimation = true;
    }
    if (advanceAnimation) {
      int frame = bounds.x + (currentFrame++ % bounds.y);
      sprite->setTexture(textures[frame]);
    }
    window.draw(*sprite);
  });
}

void SystemSortDrawabe(entt::DefaultRegistry& registry) {
  registry.sort<DrawableShape>([](const auto &lhs, const auto &rhs) {
    return lhs.z < rhs.z;
  });
}

// SYSTEM : DRAW COLLIDER
void SystemDrawCollider(RenderWindow &window, entt::DefaultRegistry &registry) {
  RectangleShape r;
  r.setFillColor(Color::Transparent);
  r.setOutlineThickness(2);
  r.setOutlineColor(Color(100, 100, 100));

  registry.view<Box2DCollider, Position>().each([&r, &window](auto entity, auto& coll, auto& pos) {
    r.setSize(Vector2f{coll.size.width, coll.size.hight});
    r.setPosition(Vector2f{pos.x, pos.y});
    window.draw(r);
  });
}

// SYSTEM : KEYBOARD FOR PADDLE
void SystemKeyboardCtrl(entt::DefaultRegistry &registry, float dt) {
  registry.view<Paddle, Position>().each([&dt](auto& entity, auto&paddle, auto& position) {
    auto moveByInPercent = Config::get().paddleVelocityRelativeToRes;
    if (Keyboard::isKeyPressed(Keyboard::Key::Right)) position.x += moveByInPercent * dt;
    if (Keyboard::isKeyPressed(Keyboard::Key::Left)) position.x -= moveByInPercent * dt;
  });

  registry.view<MainCharacter, Position, AnimationStateComponent, AnimatedSpriteComponent>().each([&dt](auto& entity, auto&mainCharacter, auto& position, auto& animationState, AnimatedSpriteComponent& animatedSprite) {
    auto moveByInPercent = Config::get().paddleVelocityRelativeToRes;
    sf::Vector2<float> input;
    if (Keyboard::isKeyPressed(Keyboard::Key::Right)) input.x += 1;
    if (Keyboard::isKeyPressed(Keyboard::Key::Left)) input.x -= 1;
    if (Keyboard::isKeyPressed(Keyboard::Key::Up)) input.y -= 1;
    if (Keyboard::isKeyPressed(Keyboard::Key::Down)) input.y += 1;

    // normalize

    float mag = std::sqrt(input.x * input.x + input.y * input.y);
    if (mag <= 0) {
      animationState.currentState = AnimationState::IDLE;
      return;
    }

    input.x /= mag;
    input.y /= mag;

    animationState.currentState = AnimationState::MOVING;
    
    //sf::Vector2<float> scale = animatedSprite.sprite->getScale();
    sf::FloatRect bounds = animatedSprite.sprite->getLocalBounds();
    int w = (int)bounds.width;
    int h = (int)bounds.height;

    // flip
    if (input.x < 0) {
      animatedSprite.sprite->setTextureRect(sf::IntRect(w, 0, -w, h));
    } else {
      animatedSprite.sprite->setTextureRect(sf::IntRect(0, 0, w, h));
    }
    position.x += input.x * moveByInPercent * dt * 0.5f;
    position.y += input.y * moveByInPercent * dt * 0.5f;
  });
}

void SystemBallWallCollisions(entt::DefaultRegistry &registry) {
  auto view = registry.view<Circle, Position, Velocity, Box2DCollider>();
  for(auto entity : view) {
    auto& config = Config::get();
    Position& position = view.get<Position>(entity);
    Velocity& velocity = view.get<Velocity>(entity);
    Box2DCollider& collider = view.get<Box2DCollider>(entity);

    if (position.x   < 0) {
//      std::cout << "GO TO THE RIGHT BATCH!"<< std::endl;
      velocity.dx = config.ballVelocity * config.displayHight1Percent;
      std::cout << " "  + std::to_string(velocity.dx) <<  " " <<  std::to_string(position.x) << std::endl;
    }
    else if (position.x + collider.size.width  > config.windowSizeX) {
//      std::cout << "GO TO THE LEFT BATCH!"<< std::endl;
      velocity.dx = -(config.ballVelocity * config.displayHight1Percent);
      std::cout << " "  + std::to_string(velocity.dx) <<  " " <<  std::to_string(position.x) << std::endl;
    }
    if (position.y  < 0) {
//      std::cout << "GO DOWN BATCH!"<< std::endl;
      velocity.dy = config.ballVelocity * config.displayHight1Percent;
      std::cout << " "  + std::to_string(velocity.dy) <<  " " <<  std::to_string(position.y) << std::endl;
    }
    else if (position.y + collider.size.hight > config.windowSizeY) {
//      registry.reset<DrawableShape>();
      // CreateGameOverPage(registry);
//      std::cout << "GO Up BATCH! " << std::to_string(position.y + collider.size.hight / 2)<< std::endl;
      std::cout << " "  + std::to_string(velocity.dy) <<  " " <<  std::to_string(position.y) << std::endl;
      velocity.dy = - (config.ballVelocity * config.displayHight1Percent);
    }
  }
}

void SystemBallCollision(entt::DefaultRegistry &registry){
  auto ball = registry.attachee<BallTag>();
  auto& ballPos = registry.get<Position>(ball);
  auto& ballVelocity = registry.get<Velocity>(ball);
  auto& ballColl = registry.get<Box2DCollider>(ball);

  auto paddle = registry.attachee<PaddleTag>();
  auto& paddlePos = registry.get<Position>(paddle);
  auto& paddleColl = registry.get<Box2DCollider>(paddle);

  auto left = RectWrapper(ballPos, ballColl.size);
  auto right = RectWrapper(paddlePos, paddleColl.size);

  if (isIntersecting(left, right)) {
    auto hitPos = CalculateCollisionPoint(paddlePos, paddleColl, ballPos, ballColl);
    std::cout << " HitPos " << std::to_string(hitPos)  << "\n";
    auto directionVector = CalculateDirectionVector(hitPos);
    std::cout << " X " << std::to_string(directionVector.x) << " Y " << std::to_string(directionVector.y) << "\n";
    ballVelocity.dx = directionVector.x * Config::get().ballVelocityRelativeToRes;
    ballVelocity.dy = directionVector.y * Config::get().ballVelocityRelativeToRes;
  }
}

void SystemBrickBallCollision(entt::DefaultRegistry &registry) {
  auto ball = registry.attachee<BallTag>();
  auto& ballPos = registry.get<Position>(ball);
  auto& ballVelocity = registry.get<Velocity>(ball);
  auto& ballColl = registry.get<Box2DCollider>(ball);
  auto ballRect = RectWrapper(ballPos, ballColl.size);

  registry.view<Brick, Box2DCollider, Position>().each([&](auto entity, auto& brick, auto& coll, auto& position) {
    auto brickRect = RectWrapper(position, coll.size);
    if (testCollision(ballRect, brickRect, ballVelocity, Config::get().ballVelocityRelativeToRes)) {
      registry.destroy(entity);
    }
  });

}

void SystemCollision(entt::DefaultRegistry& registry){
  auto withColliders = registry.view<Box2DCollider, Position>();
  registry.view<Box2DCollider, Position>().each([&withColliders, &registry](auto& entity, auto& coll, auto& position) {
    for(auto entity: withColliders) {
      auto& boxCollOther = withColliders.get<Box2DCollider>(entity);
      auto& posOther = withColliders.get<Position>(entity);
      auto left = RectWrapper(position, coll.size);
      auto right = RectWrapper(posOther, boxCollOther.size);
      if (&posOther != &position){
        if (isIntersecting(left, right)) {
          registry.assign<CoinFound>(entity);
          std::cout << "upsy, collision " << "\n";
        }
      }
    }
  });
}

void SystemClicked(RenderWindow &window, entt::DefaultRegistry &registry)
{
  auto mousePos = sf::Mouse::getPosition(window);
  registry.view<Box2DCollider, Position, ClickableShape>().each([&mousePos](auto &entity, auto &box2DCollider, auto &position, auto &clickableShape) {
    auto rect = RectWrapper(position, box2DCollider.size);
    if (rect.Top() <= mousePos.y && rect.Bottom() >= mousePos.y && rect.Left() <= mousePos.x && rect.Right() >= mousePos.x) {
      clickableShape.containsMouse = true;
      clickableShape.isClicked = sf::Mouse::isButtonPressed(sf::Mouse::Left);
      std::cout << "HERE: " << mousePos.x << ", " << mousePos.y << ";" << std::endl;
    } else {
      clickableShape.containsMouse = false;
      clickableShape.isClicked = false;
    }
  });
}

void SystemStartGame(entt::DefaultRegistry& registry) {
  for (int y = 0; y < Config::get().brickNumberY; ++y) {
    for (int x = 0; x < Config::get().brickNumberX; ++x) {
      auto entityBrick = CreateBrick(registry, x * (Config::get().brickSizeX + Config::get().brickPaddingX),
                                     y * (Config::get().brickSizeY + Config::get().brickPaddingY));
    }
  }
  auto circle = CreateBall(registry);
  auto paddle = CreatePaddle(registry);


  // CREATE MAP
  const float TILE_SCALE = 4.0f;
  const float TILE_OFFSET = 4.0f;
  static sf::Texture textureMap;
  textureMap.loadFromFile("../resources/sprites/environment/sheet.png");
  for(int x = 0; x < 8; x++) {
    for(int y = 0; y < 8; y++) {
      CreateMapTile(registry, textureMap, 6, 1, x * (TILE_SCALE * 16 + TILE_OFFSET), y * (TILE_SCALE * 16 + TILE_OFFSET), TILE_SCALE);
    }
  }

  // CREATE ATTACKING TURTLE
  const float TURTLE_SCALE = 4.0f;
  std::vector<sf::Texture> texturesForSprite;

  std::map<AnimationState, sf::Vector2i> animationStates;
  std::vector<std::string> paths;

  int offset = 0;
  int count = 0;
  auto LoadTextures = [&](std::string path, std::vector<sf::Texture>& sprites) {
    // find all textures inside of the path
    std::vector<std::string> paths;
    for (const auto & entry : fs::directory_iterator(path)) {
      if (entry.exists()) {
        paths.push_back(entry.path().string());
      }
    }
    std::sort(paths.begin(), paths.end());
  
    offset += count;
    count = 0;
    for(const auto& path : paths) {
      sf::Texture tmp;
      tmp.loadFromFile(path);
      sprites.push_back(tmp);
      count++;
    }
  };

  // sprint textures 
  LoadTextures("../resources/sprites/turtle/sprint", texturesForSprite);
  animationStates[AnimationState::MOVING] = sf::Vector2i(offset, count);

  // attacking textures
  LoadTextures("../resources/sprites/turtle/attackFinisher1", texturesForSprite);
  animationStates[AnimationState::ATTACKING] = sf::Vector2i(offset, count);

  // idle textures
  LoadTextures("../resources/sprites/turtle/idle", texturesForSprite);
  animationStates[AnimationState::IDLE] = sf::Vector2i(offset, count);

  CreateAnimatedSprite(registry, texturesForSprite, animationStates, 160.0f, 160.0f, 100.0f, TURTLE_SCALE, true);
}