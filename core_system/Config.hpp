#ifndef GAMEENGINE_CONFIG_HPP
#define GAMEENGINE_CONFIG_HPP

struct Config {
protected:
  Config() {
    displayWidth1Percent = windowSizeX/100.f;
    displayHight1Percent = windowSizeY/100.f;
    ballVelocityRelativeToRes = ballVelocity * displayHight1Percent;
    paddleVelocityRelativeToRes = paddleVelocity * displayWidth1Percent;
  }

public:
  static Config &get() {
    static Config database;
    return database;
  }

  Config(Config const &) = delete;
  Config(Config &&) = delete;
  Config &operator=(Config const &) = delete;
  Config &operator=(Config &&) = delete;
  unsigned windowSizeX {1920};
  unsigned windowSizeY {1080};
  unsigned paddleSizeX {100};
  unsigned paddleSizeY {20};
  unsigned brickSizeX {40};
  unsigned brickSizeY {40};
  unsigned brickPaddingX {8};
  unsigned brickPaddingY {8};
  unsigned brickNumberX {8};
  unsigned brickNumberY {8};
  unsigned ballRadius {10};
  unsigned ballVelocity {40};
  unsigned paddleVelocity {40};
  float ballVelocityRelativeToRes;
  float paddleVelocityRelativeToRes;
  float displayWidth1Percent;
  float displayHight1Percent;
};

#endif //GAMEENGINE_CONFIG_HPP
