#include <iostream>
#include <memory>
#include "third_party/entt/entt.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "core_system/Config.hpp"
#include "Components.hpp"
#include "Systems.hpp"
#include "Prefabs.hpp"

using namespace sf;
using entity_type = entt::DefaultRegistry::entity_type;
    sf::SoundBuffer buf;
    sf::Sound sound;


void UpdateRenderingSystems(entt::DefaultRegistry& registry, RenderWindow& window, const Time& dt) {
  SystemSortDrawabe(registry);
  //SystemDraw(window, registry);
  //SystemDrawCollider(window, registry);
  SystemClicked(window, registry);
  SystemDrawSprites(window, registry);
  SystemDrawAnimatedSprites(window, registry, dt.asMilliseconds());
}

void UpdateSimulationSystems(entt::DefaultRegistry& registry, const Time& dt) {
  //SystemMove(dt.asSeconds(), registry);
  SystemKeyboardCtrl(registry, dt.asSeconds());
  SystemApply(registry);
  //SystemBallWallCollisions(registry);
  //SystemBallCollision(registry);
  //SystemBrickBallCollision(registry);
  //SystemCollision(registry);
  SystemPlaySound(registry);
}

//SINGELTON SETTINGS -> window size, window position
int main(int argc, const char **argv) {
  entt::DefaultRegistry registry;
  RenderWindow window({Config::get().windowSizeX, Config::get().windowSizeY}, "Okoschko");
  window.setFramerateLimit(60);
  sf::Clock deltaClock;

  SystemStartGame(registry);
  Event event;
  while (window.isOpen()) {
    if (Keyboard::isKeyPressed(Keyboard::Key::Q)) break;
    while (window.pollEvent(event)) {
      if (event.type == Event::Closed) window.close();
    }

    window.clear(Color(159,159,159));
    auto dt = deltaClock.restart();
    UpdateSimulationSystems(registry, dt);
    UpdateRenderingSystems(registry, window, dt);
    window.display();
  }

  return 0;
}
