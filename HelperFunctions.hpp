#ifndef GAMEENGINE_HELPERFUNCTIONS_HPP
#define GAMEENGINE_HELPERFUNCTIONS_HPP

#include "Components.hpp"

template <class T1, class T2>
bool isIntersecting(T1& mA, T2& mB) {
  return mA.Right() >= mB.Left() && mA.Left() <= mB.Right() &&
         mA.Bottom() >= mB.Top() && mA.Top() <= mB.Bottom();
}

template <class Brick, class Ball>
bool testCollision(Ball& mBall, Brick& mBrick, Velocity& ballVelocityObj, float ballVelocity) {
  if(!isIntersecting(mBrick, mBall)) return false;

  float overlapLeft{mBall.Right() - mBrick.Left()};
  float overlapRight{mBrick.Right() - mBall.Left()};
  float overlapTop{mBall.Bottom() - mBrick.Top()};
  float overlapBottom{mBrick.Bottom() - mBall.Top()};

  bool ballFromLeft(abs(overlapLeft) < abs(overlapRight));
  bool ballFromTop(abs(overlapTop) < abs(overlapBottom));

  float minOverlapX{ballFromLeft ? overlapLeft : overlapRight};
  float minOverlapY{ballFromTop ? overlapTop : overlapBottom};

  if(abs(minOverlapX) < abs(minOverlapY))
    ballVelocityObj.dx = ballFromLeft ? -ballVelocity : ballVelocity;
  else
    ballVelocityObj.dy = ballFromTop ? -ballVelocity : ballVelocity;
  return true;
}

float CalculateCollisionPoint(Position posPaddle, Box2DCollider coll, Position posBall, Box2DCollider collBall);

struct Vector2{
  float x;
  float y;
  Vector2 operator-(const Vector2& other);
  Vector2 operator+(const Vector2& other);
  Vector2 operator*(const float scale);
};

static Vector2 up = Vector2{0.0f,1.0f};

Vector2 Lerp(Vector2 start, Vector2 end, float percent);

Vector2 CalculateDirectionVector(float hitPos);

#endif //GAMEENGINE_HELPERFUNCTIONS_HPP
