#include "Prefabs.hpp"

unsigned int CreateBrick(entt::DefaultRegistry &registry, int x, int y) {
  static int counter = 0;
  auto entity = registry.create();
  auto& config = Config::get();
  registry.assign<Position>(entity, 16.0f + x, 16.0f + y);
  registry.assign<Velocity>(entity, 0.f, 0.0f);
  registry.assign<Size>(entity, (float) config.brickSizeX, (float) config.brickSizeY);
  registry.assign<DrawableShape>(entity, std::make_shared<sf::RectangleShape>(
    sf::Vector2f{(float) config.brickSizeX, (float) config.brickSizeY}), sf::Color(223, 223, 223));
  registry.assign<Name>(entity, "brick");
  registry.assign<Brick>(entity);
  registry.assign<Box2DCollider>(entity, Size{(float) config.brickSizeX, (float) config.brickSizeY});
  return entity;
}

unsigned int CreateBall(entt::DefaultRegistry &registry) {
  auto entity = registry.create();
  auto& config = Config::get();
  auto speed = config.displayWidth1Percent * config.ballVelocity;
  registry.assign<Position>(entity, 300.0f, 300.0f );
  registry.assign<Size>(entity, (float)config.ballRadius, (float)config.ballRadius);
  registry.assign<Velocity>(entity, speed/2, -speed/2);
  registry.assign<DrawableShape>(entity, std::make_shared<sf::CircleShape>(config.ballRadius));
  registry.assign<Name>(entity, "circle");
  registry.assign<Box2DCollider>(entity, Size{(float)config.ballRadius * 2, (float)config.ballRadius * 2});
  registry.assign<Circle>(entity);
  registry.attach<BallTag>(entity);
  return entity;
}
unsigned int CreatePaddle(entt::DefaultRegistry &registry) {
  auto entity = registry.create();
  auto& config = Config::get();
  auto paddingBottom = 25;
  auto posX = config.windowSizeX/2 - config.paddleSizeX/2;
  auto posY = config.windowSizeY - config.paddleSizeY - paddingBottom;
  registry.assign<Position>(entity, (float)posX, (float)posY);
  registry.assign<Size>(entity, (float)config.paddleSizeX, (float)config.paddleSizeY);
  registry.assign<DrawableShape>(entity, std::make_shared<sf::RectangleShape>(sf::Vector2f{(float)config.paddleSizeX, (float)config.paddleSizeY}));
  registry.assign<Name>(entity, "paddle");
  registry.assign<Box2DCollider>(entity, Size{(float)config.paddleSizeX, (float)config.paddleSizeY});
  registry.assign<Paddle>(entity);
  registry.attach<PaddleTag>(entity);
  return entity;
}

unsigned int CreateGameOverPage(entt::DefaultRegistry &registry) {
  std::cout << "Game Over biatch" << std::endl;
  auto gameOverEntity = registry.create();
  auto size = sf::IntRect(0, 0, 800, 400);
  auto shape = std::make_shared<sf::RectangleShape>(sf::Vector2f{(float)size.width, (float)size.height});
  auto texture = std::make_shared<sf::Texture>();
  texture->loadFromFile("../resources/you-suck.jpg", size);
  shape->setTexture(texture.get());
  registry.assign<DrawableShape>(gameOverEntity, shape, sf::Color::White, 1);
  registry.assign<Texture2D>(gameOverEntity, texture, sf::RectangleShape{});
  registry.assign<GameOverScreen>(gameOverEntity);
  return gameOverEntity;
}

unsigned int CreateAnimatedSprite(entt::DefaultRegistry &registry, const std::vector<sf::Texture>& textures, std::map<AnimationState, sf::Vector2i> animationStates, float x, float y, float speed, float scale, bool mainChar) {
  auto sprite = std::make_shared<sf::Sprite>();
  sprite->setTexture(textures[0]);
  auto bounds = sprite->getGlobalBounds();
  sprite->setPosition(x - bounds.width * 0.5f, y - bounds.height * 0.5f);
  sprite->scale(scale, scale);

  auto entity = registry.create();
  registry.assign<Position>(entity, x, y);
  registry.assign<Name>(entity, "animatedSprite");
  registry.assign<AnimatedSpriteComponent>(entity, textures, sprite, 0, speed, 0);
  registry.assign<AnimationStateComponent>(entity, animationStates, AnimationState::IDLE);
  registry.assign<Size>(entity, bounds.width, bounds.height);
  registry.assign<Box2DCollider>(entity, Size{bounds.width, bounds.height});
  registry.assign<ClickableShape>(entity, false, false);
  if (mainChar) {
    registry.assign<MainCharacter>(entity);
  }
  // registry.assign<Size>(entity, 256.0f, 256.0f);
  // registry.assign<Box2DCollider>(entity, Size{(float)config.ballRadius * 2, (float)config.ballRadius * 2});
  return entity;
}

unsigned int CreateEnemy(entt::DefaultRegistry &registry) {
  auto entity = registry.create();
  // auto& config = Config::get();
  // registry.assign<Position>(entity, 300.0f, 300.0f );
  // registry.assign<Size>(entity, (float)config.ballRadius, (float)config.ballRadius);
  // registry.assign<DrawableShape>(entity, std::make_shared<sf::RectangleShape>(config.ballRadius));
  // registry.assign<Name>(entity, "circle");
  // registry.assign<Box2DCollider>(entity, Size{(float)config.ballRadius * 2, (float)config.ballRadius * 2});
  // registry.assign<Circle>(entity);
  // registry.attach<BallTag>(entity);
  return entity;
}

#define MAP_TILE_SIZE 16
unsigned int CreateMapTile(entt::DefaultRegistry &registry, sf::Texture& textureMap, int tileIndexX, int tileIndexY, float x, float y, float scale) {
  auto spriteSheetMap = std::make_shared<sf::Sprite>();
  spriteSheetMap->setTexture(textureMap);
  spriteSheetMap->setPosition(x, y);
  spriteSheetMap->setTextureRect(sf::IntRect(tileIndexX * MAP_TILE_SIZE, tileIndexY * MAP_TILE_SIZE, MAP_TILE_SIZE, MAP_TILE_SIZE));
  spriteSheetMap->scale(scale, scale);

  auto entity = registry.create();
  auto& config = Config::get();
  registry.assign<Position>(entity, x, y);
  registry.assign<Name>(entity, "mapTile");
  registry.assign<SpriteComponent>(entity, spriteSheetMap, 0);
  // registry.assign<Size>(entity, (float)config.brickSizeX, (float)config.brickSizeY);
  // registry.assign<DrawableShape>(entity, std::make_shared<sf::RectangleShape>(sf::Vector2f{40.f,20.0f}), sf::Color(223,223,223));
  // registry.assign<Box2DCollider>(entity, Size{(float)config.brickSizeX, (float)config.brickSizeY});
  return entity;
}