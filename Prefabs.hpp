#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "Components.hpp"
#include "third_party/entt/entt.hpp"
#include "core_system/Config.hpp"

unsigned int CreateBrick(entt::DefaultRegistry &registry, int x = 0, int y = 0);

unsigned int CreateBall(entt::DefaultRegistry &registry);

unsigned int CreatePaddle(entt::DefaultRegistry &registry);

unsigned int CreateGameOverPage(entt::DefaultRegistry &registry);

unsigned int CreateEnemy(entt::DefaultRegistry &registry);

unsigned int CreateAnimatedSprite(entt::DefaultRegistry &registry, const std::vector<sf::Texture>& textures, std::map<AnimationState, sf::Vector2i> animationStates, float x, float y, float speed, float scale, bool mainChar);

unsigned int CreateMapTile(entt::DefaultRegistry &registry, sf::Texture& textureMap, int tileIndexX, int tileIndexY, float x, float y, float scale = 1.0f);