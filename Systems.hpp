#ifndef GAMEENGINE_SYSTEMS_HPP
#define GAMEENGINE_SYSTEMS_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include "third_party/entt/entt.hpp"
#include "Components.hpp"
#include "HelperFunctions.hpp"

using namespace sf;
using entity_type = entt::DefaultRegistry::entity_type;

//SYSTEM : MOVE
void SystemMove(float dt, entt::DefaultRegistry& registry);

// SYSTEM : APPLY CHANGES
void SystemApply(entt::DefaultRegistry& registry);

// SYSTEM : DRAW
void SystemDraw(RenderWindow& window, entt::DefaultRegistry& registry);

// SYSTEM : DRAW COLLIDER
void SystemDrawCollider(RenderWindow& window, entt::DefaultRegistry& registry);

// SYSTEM : KEYBOARD FOR PADDLE
void SystemKeyboardCtrl(entt::DefaultRegistry& registry, float dt);

void SystemBallWallCollisions(entt::DefaultRegistry& registry);

void SystemBallCollision(entt::DefaultRegistry& registry);

void SystemBrickBallCollision(entt::DefaultRegistry& registry);

void SystemCollision(entt::DefaultRegistry& registry);

void SystemClicked(RenderWindow& window, entt::DefaultRegistry& registry);

void SystemStartGame(entt::DefaultRegistry& registry);

void SystemSortDrawabe(entt::DefaultRegistry& registry);

void SystemPlaySound(entt::DefaultRegistry& registry);

void SystemDrawSprites(RenderWindow &window, entt::DefaultRegistry& registry);

void SystemDrawAnimatedSprites(RenderWindow &window, entt::DefaultRegistry& registry, float dt);

#endif //GAMEENGINE_SYSTEMS_HPP
